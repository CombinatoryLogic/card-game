function isObject(x) {
	return typeof x === 'object' && x !== null;
}

function createGrid(className, text) {
	let grid = document.createElement("div");
	grid.className = className;

	if (Array.isArray(text)) {
		for (var textChild of text) {
			let child = createGrid("", textChild);
			grid.appendChild(child);
		}
	} else
		grid.innerHTML = "<div>"+text+"</div>";
	return grid;
}
/*
function createGrid1(type, text)
{
	let grid = document.createElement("div");
	grid.className = "grid "+type;
	for (var textRow of text) {
		let row = document.createElement("div");
		for (var textCell of textRow) {
			let cell = document.createElement("div");
			cell.innerHTML = "<div>"+textCell+"</div>";
			row.appendChild(cell);
		}
		grid.appendChild(row);
	}
	return grid;
}

function createCard(type = "",
				middleCenter = "", img = "",
				topCenter = "", bottomCenter = "",
				topLeft = "", topRight = "",
				bottomLeft = "", bottomRight = "",
				middleLeft = "", middleRight = "")
{
	let cardE = document.createElement("div");
	cardE.className = "grid "+type;


	let topE = document.createElement("div");
	topE.className = "top-row";

	let middleE = document.createElement("div");
	middleE.className = "middle-row";

	let bottomE = document.createElement("div");
	bottomE.className = "bottom-row";


	let topLeftE = document.createElement("div");
	topLeftE.className = "top left";

	let topCenterE = document.createElement("div");
	topCenterE.className = "top center";

	let topRightE = document.createElement("div");
	topRightE.className = "top right";
	let middleLeftE = document.createElement("div");
	middleLeftE.className = "middle left";

	let middleCenterE = document.createElement("div");
	middleCenterE.className = "middle center";

	let middleRightE = document.createElement("div");
	middleRightE.className = "middle right";
	let bottomLeftE = document.createElement("div");
	bottomLeftE.className = "bottom left";
	let bottomCenterE = document.createElement("div");
	bottomCenterE.className = "bottom center";
	let bottomRightE = document.createElement("div");
	bottomRightE.className = "bottom right";


	topLeftE.innerHTML = "<div>"+topLeft+"</div>";
	topCenterE.innerHTML = "<div>"+topCenter+"</div>";
	topRightE.innerHTML = "<div>"+topRight+"</div>";
	middleLeftE.innerHTML = "<div>"+middleLeft+"</div>";
	middleCenterE.innerHTML = "<div>"+middleCenter+"</div>";
	middleRightE.innerHTML = "<div>"+middleRight+"</div>";
	bottomLeftE.innerHTML = "<div>"+bottomLeft+"</div>";
	bottomCenterE.innerHTML = "<div>"+bottomCenter+"</div>";
	bottomRightE.innerHTML = "<div>"+bottomRight+"</div>";


	cardE.appendChild(topE);
	topE.appendChild(topLeftE);
	topE.appendChild(topCenterE);
	topE.appendChild(topRightE);
	cardE.appendChild(middleE);
	middleE.appendChild(middleLeftE);
	middleE.appendChild(middleCenterE);
	middleE.appendChild(middleRightE);
	cardE.appendChild(bottomE);
	bottomE.appendChild(bottomLeftE);
	bottomE.appendChild(bottomCenterE);
	bottomE.appendChild(bottomRightE);

	return cardE;
}*/